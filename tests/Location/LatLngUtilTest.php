<?php
namespace Showtheway\Tests\Location;

use Showtheway\Location\LatLngUtil;

/**
 * Class LatLngUtilTest
 * @package Showtheway\Tests\Location
 */
class LatLngUtilTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider sanitizeProvider
     * @param string $ll
     * @param string $expected
     */
    public function testSanitize($ll, $expected)
    {
        $stub = new LatLngUtil();
        $actual = $stub->sanitize($ll);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function sanitizeProvider()
    {
        return [
            'zero' => ['0,0', '0,0'],
            'north-west' => ['-90.000000,-180.000000', '-90,-180'],
            'north-east' => ['-90.000000,180.000000', '-90,180'],
            'south-west' => ['90.000000,-180.000000', '90,-180'],
            'south-east' => ['90.000000,180.000000', '90,180'],
            'theyogicscience' => ['3.053864,101.688240', '3.053864,101.68824'],
            'empty' => ['', ''],
            'only-lat' => ['90,', ''],
            'only-lng' => [',90', ''],
            'large-lat' => ['100,100', ''],
            'large' => ['1000,1000', ''],
            'non-numeric' => ['arief,manju', ''],
        ];
    }
}
