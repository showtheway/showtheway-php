<?php
namespace Showtheway\Location;

/**
 * Class LatLngUtil
 * @package Showtheway\Location
 */
class LatLngUtil
{
    /**
     * @param double $value
     * @param double $min
     * @param double $max
     * @return bool
     */
    public function isInRange($value, $min, $max)
    {
        return ($value >= $min) && ($value <= $max);
    }

    /**
     * @param double $lat
     * @param double $lng
     * @return bool
     */
    public function isValid($lat, $lng)
    {
        return is_numeric($lat) && is_numeric($lng) && $this->isInRange($lat, -90, 90) &&
        $this->isInRange($lng, -180, 180);
    }

    /**
     * @param string $ll
     * @return string
     */
    public function sanitize($ll)
    {
        $lls = explode(',', $ll);
        foreach ([0, 1] as $k) {
            $lls[$k] = isset($lls[$k]) && is_numeric($lls[$k]) ? doubleval($lls[$k]) : null;
        }
        $ll = '';
        if ($this->isValid($lls[0], $lls[1])) {
            $ll = round($lls[0], 6) . ',' . round($lls[1], 6);
        }

        return $ll;
    }
}
