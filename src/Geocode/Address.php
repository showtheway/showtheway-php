<?php
namespace Showtheway\Geocode;

/**
 * Class Address
 * @package Showtheway\Geocode
 */
class Address
{
    /**
     * @var string
     */
    public $name = '';
    /**
     * @var string
     */
    public $street = '';
    /**
     * @var string
     */
    public $locality = '';
    /**
     * @var string
     */
    public $region = '';
    /**
     * @var string
     */
    public $postalCode = '';
    /**
     * @var string
     */
    public $country = '';
    /**
     * @var string
     */
    public $countryCode = '';
    /**
     * @var string
     */
    public $formatted = '';

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->formatted;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->name,
            'street' => $this->street,
            'locality' => $this->locality,
            'region' => $this->region,
            'postal_code' => $this->postalCode,
            'country' => $this->country,
            'country_code' => $this->countryCode,
            'formatted' => $this->formatted,
        ];
    }
}
